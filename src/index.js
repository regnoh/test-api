import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import axios from 'axios'
import Vueaxios from 'vue-axios'
import './components/Gleaf'
// use
Vue.use(mavonEditor)
Vue.use(Vueaxios, axios)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  data() {
    return { value: '' }
  }
}).$mount('#app')