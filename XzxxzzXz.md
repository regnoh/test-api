# doc-platform

> 帮助中心1.0

## 运行步骤

```bash
# install dependencies
$ cnpm install

# serve with hot reload at localhost:3000
$ cnpm run dev
```
# 一. 框架构建

## 1. nuxt
[Nuxt.js docs](https://nuxtjs.org)

## 2. css
1. 安装
```bash
cnpm i node-sass sass-loader -D
```
2. style lang='scss' scoped> 即可使用scss语法

## 3. ui
[gleaf](http://gleaf.yealink.com)
1. 安装
```bash
cnpm i gleaf gleaf-extend -S
```
2. 在plugins 新建 gleaf.js, 引入组件
3. plugins.js 配置 css & plugins
4. 在.vue中即可使用组件<yl-button>

## 4. axios
1. 安装
```bash
cnpm install @nuxtjs/axios axios -S
```
2. 在plugins 新建 axios.js, 设置拦截器等
3. plugins.js 配置 plugins,
4. 在 .vue 中即可使用 axios： this.$axios.get(apiUrl, params: {})

# 业务实现
## 5. mavon-editor
[mavonEditor](https://github.com/hinesboy/mavonEditor/blob/master/README.md)
1. 安装
```bash
cnpm install mavon-editor -S
```
2. 在 plugins 新建 vue-mavon-editor.js, 引入组件
3. plugins.js 配置 css & plugins
4. 在 .vue 中即可使用 <mavon-editor />
5. 预览
> 段落引用单栏 预览 不显示工具栏
```html
<mavon-editor 
  v-model="currentFile" 
  :subfield='false' 
  defaultOpen='preview'  
  :toolbarsFlag='false' />
```
6. 编辑
> 默认： 双栏 编辑+预览 显示工具栏
```html
<mavon-editor 
  v-model="currentFile"
  @save="editFile"
  @change='changeFile' />
```
## 6. Tree
1. 实现左侧 文章目录树
> 文件夹： 点击展开收缩子文件
> 段落引用文件： 跳转到相应的文件详情
2. 引入组件
```js
import {
  YlTree
} from 'gleaf';
Vue.use(YlTree);
```
3. 使用组件
```html
<el-tree
 :data="tree"
 @node-click="handleNodeClick" />
```
## 7. 获取gitlab文章
1. 参考网址
[gitlab api](https://docs.gitlab.com/ce/api/)
2. 主要步骤
> 在gitlab创建项目和md文件（eg: test-api/README.md）
> 接口的基本路径
"<替换成你的实际地址>/api/v4/"， 例如"https://gitlab.com/api/v4/" 或者 "http://gitcode.yealink.com/api/v4/"
> 创建[访问令牌](创建访问令牌)（后面访问api会用到）
> 复制令牌或者调用api获取
